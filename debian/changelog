planetblupi (1.14.2-3) unstable; urgency=medium

  * Run wrap-and-sort -baskt
  * S-V: Update to 4.6.0 without changes needed
  * Bump debhelper compat to 13
  * Add Salsa CI configuration
  * Remove myself from Uploaders

 -- Didier Raboud <odyx@debian.org>  Thu, 02 Sep 2021 16:46:57 +0200

planetblupi (1.14.2-2) unstable; urgency=medium

  [ Simon McVittie ]
  * d/p/Avoid-making-assumptions-about-location-of-SDL2-headers.patch:
    Don't assume that SDL2 headers are in /usr/include/SDL2
    (Closes: #952105)

  [ Didier Raboud ]
  * lintian-brush: Set some upstream metadata fields
  * Bump Standards-Version to 4.5.0, cleanup versions

 -- Didier Raboud <odyx@debian.org>  Mon, 09 Mar 2020 20:13:59 +0100

planetblupi (1.14.2-1) unstable; urgency=medium

  * New 1.14.2 upstream release
  * Bump S-V to 4.4.1 without changes needed
  * Set upstream metadata fields: Repository.

 -- Didier Raboud <odyx@debian.org>  Fri, 18 Oct 2019 14:12:32 +0200

planetblupi (1.14.1-2) unstable; urgency=medium

  * Move to unstable
  * Bump S-V to 4.4 without changes needed
  * Bump debhelper compat to 12
  * Cherry-pick two fixes from upstream:
    - Fix bad translation
    - Do not play the win sound while the win video is playing

 -- Didier Raboud <odyx@debian.org>  Fri, 20 Sep 2019 16:49:26 +0200

planetblupi (1.14.1-1) experimental; urgency=medium

  * New 1.14.1 upstream release

 -- Didier Raboud <odyx@debian.org>  Wed, 13 Mar 2019 21:53:25 +0100

planetblupi (1.14.0-1) experimental; urgency=low

  * New 1.14.0 upstream release

 -- Didier Raboud <odyx@debian.org>  Sun, 24 Feb 2019 15:27:22 +0100

planetblupi (1.13.2-3) unstable; urgency=medium

  * Generate and install the manpage:
    - Patch to move it to section 6 (games)
    - Add 'ronn' as Build-Dependency

 -- Didier Raboud <odyx@debian.org>  Wed, 24 Oct 2018 08:45:19 +0200

planetblupi (1.13.2-2) unstable; urgency=medium

  * Upload to unstable

 -- Didier Raboud <odyx@debian.org>  Mon, 17 Sep 2018 10:16:56 +0200

planetblupi (1.13.2-1) experimental; urgency=medium

  * New 1.13.2 release
  * Bump S-V to 4.2.1 without changes needed

 -- Didier Raboud <odyx@debian.org>  Mon, 03 Sep 2018 12:06:00 +0200

planetblupi (1.12.5-2) experimental; urgency=low

  * Adapt to libsdl-kitchensink's new API:
    - Bump libsdl-kitchensink-dev B-D to >= 1
    - Backport two patches from upstream:
      + Add SDL2 directory as include dir for SDL_kitchensink (Kit)
      + Adapt movie impl. for SDL_kitchensink1 API

 -- Didier Raboud <odyx@debian.org>  Fri, 06 Jul 2018 14:49:09 +0200

planetblupi (1.12.5-1) unstable; urgency=medium

  * New 1.12.5 release

  [ Reiner Herrmann ]
  * Use fixed serial number to build ogg files reproducibly
  * Point Vcs-* fields to salsa
  * Bump Standards-Version to 4.1.4; no changes required
  * Add watch file

  [ Didier Raboud ]
  * d/copyright update

 -- Didier Raboud <odyx@debian.org>  Fri, 08 Jun 2018 10:50:35 +0200

planetblupi (1.12.4-1) unstable; urgency=medium

  * New 1.12.4 release

 -- Didier Raboud <odyx@debian.org>  Thu, 15 Mar 2018 08:49:52 +0100

planetblupi (1.12.2-1) unstable; urgency=medium

  * New 1.12.2 upstream release
    - Drop the patches from upstream

 -- Didier Raboud <odyx@debian.org>  Tue, 06 Feb 2018 20:41:19 +0100

planetblupi (1.12.1-22-g13a7e27-3) unstable; urgency=medium

  * Cherry-pick two upstream fixes to only allow music format choices
    amongst those available

 -- Didier Raboud <odyx@debian.org>  Fri, 02 Feb 2018 19:13:04 +0100

planetblupi (1.12.1-22-g13a7e27-2) unstable; urgency=medium

  * Add missing Replaces/Breaks in the two new packages
  * Pipe timidity to sox for encoding to ogg
  * Bump S-V to 4.1.3 without changes needed

 -- Didier Raboud <odyx@debian.org>  Mon, 01 Jan 2018 10:41:23 +0100

planetblupi (1.12.1-22-g13a7e27-1) unstable; urgency=medium

  * New pre-1.12.2 snapshot; MIDI files are restored, no release yet

  * During build, convert the .mid's to .ogg's using timidity and
    timgm6mb-soundfont
  * Split data package in -music-mid and -music-ogg alternatives

 -- Didier Raboud <odyx@debian.org>  Sun, 31 Dec 2017 14:41:26 +0100

planetblupi (1.12.0-16-g222113a-1) unstable; urgency=medium

  * New pre-1.12.1 snapshot; just before the MIDI-to-Ogg conversion

 -- Didier Raboud <odyx@debian.org>  Fri, 08 Dec 2017 09:54:17 +0100

planetblupi (1.12.0-1) unstable; urgency=medium

  * New 1.12.0 upstream release
    - Remove all patches which were from upstream
    - Use @PB_ICON@ to link to the icon from the desktop file
  * Set R³ to no

 -- Didier Raboud <odyx@debian.org>  Thu, 02 Nov 2017 20:16:47 +0100

planetblupi (1.11.0-3) unstable; urgency=medium

  * Patches:
    - Install a .desktop file: backport and enhance
    - Make the CURL dependency optional: backport the enhancements
    - Use GNUInstallDirs instead of patching the BINDIR: patch update
    - Replace a char by Sint8: backport. Should fix the FTBFS'es

 -- Didier Raboud <odyx@debian.org>  Wed, 25 Oct 2017 09:11:15 +0200

planetblupi (1.11.0-2) unstable; urgency=medium

  * Backport two patch series from upstream:
    - Build a desktop file
    - Make the CURL dependency optional, through the PB_HTTP_VERSION_CHECK
      CMake option (refresh the previous attempt)

  * Use non-HTTPS homepage, the HTTPS version doesn't work
  * Install the desktop file
  * Add debian/gbp.conf

 -- Didier Raboud <odyx@debian.org>  Sun, 22 Oct 2017 11:54:10 +0200

planetblupi (1.11.0-1) unstable; urgency=low

  * Initial release. (Closes: #879214)

 -- Didier Raboud <odyx@debian.org>  Fri, 20 Oct 2017 16:57:03 +0200
